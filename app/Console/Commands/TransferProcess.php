<?php

namespace App\Console\Commands;

use App\Facades\TransferBasicService;
use Illuminate\Console\Command;

class TransferProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transfer:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process in time transfers and update statuses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        TransferBasicService::processAll();
    }
}
