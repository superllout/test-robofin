<?php

namespace App\Facades;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Facade;

/**
 * @method static Collection getAllBySenderId(int $senderId)
 * @method static Carbon getParsedPaymentTime(string $date, int $hours = null)
 * @method static array getParsedParams(array $params)
 * @method static bool isTransferAllowed(float $balance, float $amount)
 * @method static array setTransfer(int $senderId, int $receiverId, float $amount, string $date, int $hours = null)
 * @method static void processAll()
 *
 * @see \App\Services\TransferBasicService
 */

class TransferBasicService extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'service.transfer_basic';
    }
}