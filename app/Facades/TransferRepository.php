<?php

namespace App\Facades;

use App\Models\Transfer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Facade;

/**
 * @method static Transfer store($senderId, $receiverId, $amount, Carbon $paymentTime)
 * @method static Collection getAllInTime()
 * @method static Collection getAllBySenderId(int $senderId)
 * @method static void setStatusSuccess(int $transferId)
 * @method static void setStatusFault(int $transferId)
 * @method static void setStatusProcessing(array $transferIds)
 *
 * @see \App\Repositories\TransferRepository
 */

class TransferRepository extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'repository.transfer';
    }
}