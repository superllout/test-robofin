<?php

namespace App\Facades;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Facade;

/**
 * @method static float getBalanceById(int $senderId)
 * @method static Collection getReceiversById(int $userId)
 * @method static getAllWithTransferLast()
 * @method static void setBalanceSubById(int $senderId, float $amount)
 * @method static void setBalanceAddById(int $senderId, float $amount)
 *
 * @see \App\Services\UserBasicService
 */

class UserBasicService extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'service.user_basic';
    }
}