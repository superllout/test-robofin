<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Requests\AuthRequest;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function auth(AuthRequest $request)
    {
        $validated  = $request->validated();
        Auth::loginUsingId($validated['user_id']);
        return redirect('/');
    }

    public function login()
    {
        $data = ['users' => User::all()];
        return view('login', $data);
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}