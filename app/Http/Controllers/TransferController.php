<?php

namespace App\Http\Controllers;

use App\Facades\TransferBasicService;
use App\Facades\UserBasicService;
use App\Requests\TransferSubmitRequest;
use Illuminate\Support\Facades\Auth;

class TransferController extends Controller
{
    public function index()
    {
        $data['transfers'] = TransferBasicService::getAllBySenderId(Auth::id());
        return view('transfers.index', $data);
    }

    public function fill()
    {
        $result = UserBasicService::getReceiversById(Auth::id());
        return view('transfers.fill', $result);
    }

    public function submit(TransferSubmitRequest $request)
    {
        $validated = TransferBasicService::getParsedParams($request->validated());
        $result = TransferBasicService::setTransfer(Auth::id(), $validated['receiver_id'], $validated['amount'] , $validated['transfer_date'], $validated['transfer_hour']);

        if($result['status']) {
            return redirect('/transfers');
        }

        return redirect('/transfers/fill')->withErrors($result['reason']);
    }
}