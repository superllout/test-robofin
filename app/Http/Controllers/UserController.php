<?php

namespace App\Http\Controllers;

use App\Facades\UserBasicService;

class UserController extends Controller
{
    public function index()
    {
        $data = ['users' => UserBasicService::getAllWithTransferLast()];
        return view('index', $data);
    }
}