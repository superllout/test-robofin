<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    protected $table = 'transfers';

    public $timestamps = true;

    protected $fillable = [
        'sender_id', 'receiver_id', 'amount', 'payment_time', 'status_id'
    ];
}