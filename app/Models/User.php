<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $fillable = [
        'name', 'email', 'balance',
    ];

    public $casts = [
        'balance' => 'float(10,2)'
    ];

    public function transferLatest()
    {
        return $this->hasMany('App\Models\Transfer', 'sender_id')->latest()->take(1);
    }

    public static function withTransferLast()
    {
        return self::query()->leftJoin(\DB::raw('(select transfer_max_id, sender_id, receiver_id, amount, payment_time, status_id, 
            created_at as transfer_created_at, updated_at as transfer_updated_at 
            from (select max(id) transfer_max_id, sender_id as transfer_max_sender_id 
            from transfers group by transfer_max_sender_id) tr1
            left join transfers tr2 on tr1.transfer_max_id = tr2.id) trans'), 'trans.sender_id', 'users.id');
    }
}
