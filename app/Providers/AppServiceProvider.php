<?php

namespace App\Providers;

use App\Models\Transfer;
use App\Models\User;
use App\Repositories\TransferRepository;
use App\Repositories\UserRepository;
use App\Services\TransferBasicService;
use App\Services\UserBasicService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Models
        $this->app->alias(Transfer::class, 'model.transfer');
        $this->app->alias(User::class, 'model.user');

        // Repositories
        $this->app->bind(TransferRepository::class, function (){
            $transfer = app('model.transfer');
            return new TransferRepository($transfer);
        });
        $this->app->alias(TransferRepository::class, 'repository.transfer');

        $this->app->bind(UserRepository::class, function (){
            $user = app('model.user');
            return new UserRepository($user);
        });
        $this->app->alias(UserRepository::class, 'repository.user');

        // Services
        $this->app->bind(TransferBasicService::class, function(){
            $device = app('repository.transfer');
            return new TransferBasicService($device);
        });
        $this->app->alias(TransferBasicService::class, 'service.transfer_basic');

        $this->app->bind(UserBasicService::class, function(){
            $device = app('repository.user');
            return new UserBasicService($device);
        });
        $this->app->alias(UserBasicService::class, 'service.user_basic');
    }
}
