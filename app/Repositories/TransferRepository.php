<?php

namespace App\Repositories;

use App\Models\Transfer;
use Carbon\Carbon;

class TransferRepository
{
    public function __construct(Transfer $model)
    {
        $this->model = $model;
    }

    /**
     * @param int $senderId
     * @param int $receiverId
     * @param float $amount
     * @param Carbon $paymentTime
     * @return Transfer
     */
    public function store($senderId, $receiverId, $amount, Carbon $paymentTime)
    {
        $transfer = $this->model::create([
            'sender_id' => $senderId,
            'receiver_id' => $receiverId,
            'amount' => $amount,
            'payment_time' => $paymentTime->toDateTimeString(),
            'status_id' => config('transfer.status.wait')
        ]);

        return $transfer;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllInTime()
    {
        return $this->model::query()->where('status_id', config('transfer.status.wait'))->where('payment_time', '<=', Carbon::now()->toDateTimeString())->get();
    }

    /**
     * @param int $senderId
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllBySenderId(int $senderId)
    {
        return $this->model::query()->where('sender_id', $senderId)->get();
    }

    /**
     * @param int $transferId
     * @return void
     */
    public function setStatusSuccess(int $transferId)
    {
        $this->model::query()->where('id', $transferId)->update(['status_id' => config('transfer.status.success')]);
    }

    /**
     * @param int $transferId
     * @return void
     */
    public function setStatusFault(int $transferId)
    {
        $this->model::query()->where('id', $transferId)->update(['status_id' => config('transfer.status.fault')]);
    }

    /**
     * @param array $transferIds
     * @return void
     */
    public function setStatusProcessing(array $transferIds)
    {
        $this->model::query()->whereIn('id', $transferIds)->update(['status_id' => config('transfer.status.processing')]);
    }

    /**
     * @param array $transferIds
     * @return void
     */
    public function setStatusWait(array $transferIds)
    {
        $this->model::query()->whereIn('id', $transferIds)->update(['status_id' => config('transfer.status.wait')]);
    }


}