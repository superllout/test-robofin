<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository
{
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * @param int $senderId
     * @return float
     */
    public function getBalanceById(int $senderId)
    {
        return $this->model::query()->select('balance')->where('id', $senderId)->get()->pluck('balance')->first();
    }

    /**
     * @param int $senderId
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getReceiversById(int $senderId)
    {
        return $this->model::query()->where('id', '!=',$senderId)->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllWithTransferLast()
    {
        return $this->model::withTransferLast()->get();
    }

    public function setBalanceSubById(int $senderId, float $amount)
    {
        $this->model::query()->where('id', $senderId)->update(['balance' => \DB::raw("balance - $amount")]);
    }

    public function setBalanceAddById(int $senderId, float $amount)
    {
        $this->model::query()->where('id', $senderId)->update(['balance' => \DB::raw("balance + $amount")]);
    }
}