<?php

namespace App\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class TransferSubmitRequest extends FormRequest
{
    public function rules()
    {
        $balance = Auth::user()->balance;

        return [
            'sender_id' => 'required|integer|exists:users,id',
            'receiver_id' => 'required|integer|exists:users,id',
            'amount' => "required|numeric|max:$balance",
            'transfer_date' => 'required|date_format:d-m-Y',
            'transfer_hour' => "nullable|integer|min:0|max:23"
        ];
    }

    public function messages()
    {
        return [
            'sender_id.required' => 'You must select a user',
            'receiver_id.required' => 'You must select a user',
        ];
    }
}