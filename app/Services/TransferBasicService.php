<?php

namespace App\Services;

use App\Facades\UserBasicService;
use App\Repositories\TransferRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class TransferBasicService
{
    private $transferRepository;

    public function __construct(TransferRepository $transferRepository)
    {
        $this->transferRepository = $transferRepository;
    }

    /**
     * @param int $senderId
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllBySenderId(int $senderId)
    {
        return $this->transferRepository->getAllBySenderId($senderId);
    }

    /**
     * @param string $date
     * @param int|null $hours
     * @return Carbon
     */
    public function getParsedPaymentTime(string $date, int $hours = null)
    {

        if($hours === null && Carbon::parse($date) == Carbon::today())
        {
            $hours = Carbon::now()->hour + 1;
        }
        if($hours === null && Carbon::parse($date) != Carbon::today())
        {
            $hours = Carbon::now()->hour;
        }
        if($hours == 23 && Carbon::now()->hour == 23 && Carbon::parse($date) == Carbon::today())
        {
            return Carbon::today()->addDay();
        }

        return Carbon::parse($date)->addHours($hours);
    }

    /**
     * @param array $params
     * @return array
     */
    public function getParsedParams(array $params)
    {
        isset($params['transfer_hour']) ?: $params['transfer_hour'] = null;
        return $params;
    }

    /**
     * @param int $senderId
     * @param float $amount
     * @return bool
     */
    public function isTransferAllowed(int $senderId, float $amount)
    {
        $balance = UserBasicService::getBalanceById($senderId);

        if($balance - $amount >= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * @param int $senderId
     * @param int $receiverId
     * @param float $amount
     * @param string $date
     * @param int|null $hours
     * @return array
     */
    public function setTransfer(int $senderId, int $receiverId, float $amount, string $date, int $hours = null)
    {
        try
        {
            $isAllowed = $this->isTransferAllowed($senderId, $amount);
            if (!$isAllowed) {
                return ['status' => false, 'reason' => config('transfer.reason.wrong_balance')];
            }

            \DB::beginTransaction();
            UserBasicService::setBalanceSubById($senderId, $amount);

            $paymentTime = $this->getParsedPaymentTime($date, $hours);

            if(Carbon::now() > $paymentTime)
            {
                return ['status' => false, 'reason' => config('transfer.reason.wrong_time')];
            }

            $this->transferRepository->store($senderId, $receiverId, $amount, $paymentTime);

            \DB::commit();
            return ['status' => true, 'reason' => []];
        }
        catch(\Exception $exception)
        {
            \DB::rollback();
            Log::error('STORING TRANSFER: ' . $exception->getMessage());
            return ['status' => false, 'reason' => config('transfer.reason.server_error')];
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllInTime()
    {
        return $this->transferRepository->getAllInTime();
    }

    /**
     * @param int $transferId
     * @param int $receiverId
     * @param float $amount
     */
    public function processById(int $transferId, int $receiverId, float $amount)
    {
        $this->transferRepository->setStatusSuccess($transferId);
        UserBasicService::setBalanceAddById($receiverId, $amount);
    }

    /**
     * @return void
     */
    public function processAll()
    {
        $transfers = $this->getAllInTime();
        $transferIds = $transfers->pluck('id')->toArray();
        try
        {
            $this->transferRepository->setStatusProcessing($transferIds);
            \DB::beginTransaction();

            foreach ($transfers as $transfer)
            {
                $this->processById($transfer->id, $transfer->receiver_id, $transfer->amount);
            }

            \DB::commit();
        }
        catch(\Exception $exception)
        {
            \DB::rollback();
            $this->transferRepository->setStatusWait($transferIds);
            Log::error('PROCESSING TRANSFERS: ' . $exception->getMessage());
        }
    }

}