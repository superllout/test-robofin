<?php

namespace App\Services;

use App\Repositories\UserRepository;

class UserBasicService
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param int $senderId
     * @return mixed
     */
    public function getReceiversById(int $senderId)
    {
        $result['receivers'] = $this->userRepository->getReceiversById($senderId);
        return $result;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllWithTransferLast()
    {
        return $this->userRepository->getAllWithTransferLast();
    }

    /**
     * @param int $senderId
     * @return float
     */
    public function getBalanceById(int $senderId)
    {
        return $this->userRepository->getBalanceById($senderId);
    }

    /**
     * @param int $senderId
     * @param float $amount
     */
    public function setBalanceSubById(int $senderId, float $amount)
    {
        $this->userRepository->setBalanceSubById($senderId, $amount);
    }

    /**
     * @param int $senderId
     * @param float $amount
     */
    public function setBalanceAddById(int $senderId, float $amount)
    {
        $this->userRepository->setBalanceAddById($senderId, $amount);
    }
}