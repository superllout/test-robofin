<?php

return [
    'status' => [
        'success'           => 1,
        'fault'             => 2,
        'wait'              => 3,
        'processing'        => 4
    ],

    'reason' => [
        'wrong_time'        => 'You take wrong date',
        'wrong_balance'     => 'Your balance is not enough',
        'server_error'      => 'Server is temporarily not responding'
    ]
];