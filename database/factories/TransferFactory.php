<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(\App\Models\Transfer::class, function (Faker $faker) {
    return [
        'sender_id' => function () {
            return factory(User::class)->create()->id;
        },
        'receiver_id' => function () {
            return factory(User::class)->create()->id;
        },
        'payment_time' => \Carbon\Carbon::now()->subHour()->toDateTimeString(),
        'amount' => rand(300, 1500) / 10,
        'status_id' => config('transfer.status.wait'),
    ];
});
