<?php

use App\Models\Status;
use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::query()->updateOrCreate(
            [
                'id' => config('transfer.status.success'),
                'name' => 'success'
            ]
        );
        Status::query()->updateOrCreate(
            [
                'id' => config('transfer.status.fault'),
                'name' => 'fault'
            ]
        );
        Status::query()->updateOrCreate(
            [
                'id' => config('transfer.status.wait'),
                'name' => 'wait'
            ]
        );
        Status::query()->updateOrCreate(
            [
                'id' => config('transfer.status.processing'),
                'name' => 'processing'
            ]
        );
    }
}
