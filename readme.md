<p align="center">
    <img src="https://i0.wp.com/powerphrase.com/wp-content/uploads/2018/07/laravel-logo.png">
</p>

## Test Robofin Project

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:


## Server Requirements

- PHP >= 7.1.3
- BCMath PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Docker version 19.03.1
- Docker-compose version 1.24.1

## Project Start up

Clone project and copy `.env-example` to `.env`
```console
cp .env-example .env
```
Clone project and go to `docker` folder
```console
cd docker
```
Copy `.env-example` file to `.env` file
```console
cp .env-example .env
```
Build docker images.

**WARNING: docker commands must be executed with root privileges**
```console
docker-compose build
```
Start up docker-containers
```console
docker-compose up -d
```

By default nginx in docker-container starts up on 80 port. But you can change `PORT_NGINX` in `.env` file

Check php container by command
```console
docker ps
```
And get `CONTAINER ID` in output

Run commands to download vendors, migrate database and seed tables (`CONTAINER_ID` in commands you must replace with actual php container ID )
```console
docker exec -w /var/www/html/robofin -it CONTAINER_ID  composer install
docker exec -w /var/www/html/robofin -it CONTAINER_ID  php artisan key:generate
docker exec -w /var/www/html/robofin -it CONTAINER_ID  php artisan migrate
docker exec -w /var/www/html/robofin -it CONTAINER_ID  php artisan db:seed
```

Now you can go to localhost:80 in you browser.

To execute tests just run
```console
docker exec -w /var/www/html/robofin -it CONTAINER_ID  vendor/bin/phpunit
```

To process saved transfers run
```console
docker exec -w /var/www/html/robofin -it CONTAINER_ID  php artisan transfer:process
```

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
