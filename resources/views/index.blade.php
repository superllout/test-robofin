@extends('app')
@section('content')
    <div class="table-responsive">
        <table class="table">
            <caption>List of users and last transfers</caption>
            <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Balance</th>
                <th scope="col">Last transfer amount</th>
                <th scope="col">Last payment time</th>
                <th scope="col">Last transfer status</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->balance }}</td>
                    <td>{{ $user->amount }}</td>
                    <td>{{ $user->payment_time }}</td>
                    <td>{{ $user->status_id }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection