@extends('app')
@section('content')
    <form  method="POST" action="/auth">
        {{ csrf_field() }}

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row">
        <div class="col-md-4">
        </div>
        <select class="custom-select custom-select-lg col-md-4" name="user_id">
            <option selected disabled>Select user please</option>
            @foreach($users as $user)
                <option value={{$user->id}}>{{$user->name}}</option>
            @endforeach
        </select>
        <div class="col-md-4"></div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <input class="btn btn-primary users_sbmt col-md-4" type="submit" value="Submit">
            <div class="col-md-4"></div>
        </div>
    </form>
@endsection