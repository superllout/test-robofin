@extends('app')
@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">You are {{ Auth::user()->name }}</h1>
            <p class="lead">Your balance is {{ Auth::user()->balance }} rubles. You can't transfer more.</p>
        </div>
    </div>

    <form  method="POST" action="/transfers/submit">
        {{ csrf_field() }}
        <input id="sender_id" name="sender_id" type="hidden" value={{ Auth::user()->id }}>
        <input id="sender_balance" name="sender_balance" type="hidden" value={{ Auth::user()->balance }}>
        <div class=" form-group row">
            <div class="col-md-4"></div>
            <div class=" col-md-4">
                <label for="transferAmount">Transfer Amount</label>
                <input type="number" step="0.01" class="form-control custom-select-lg" id="transferAmount" aria-describedby="emailHelp" placeholder="Transfer Amount" name="amount">
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="form-group row">
            <div class="col-md-4"></div>
            <div class=" col-md-2">
                <label for="transferDate">Transfer Date</label>
                <input class="date form-control custom-select-lg" id="transferDate" type="text" placeholder="Transfer Date" name="transfer_date">
            </div>
            <div class="col-md-1">
            <label for="transferHour">Transfer Hour</label>
            <select id="transferHour" class="custom-select custom-select-lg" name="transfer_hour">
                <option selected disabled>hour</option>
                @for($i=0; $i < 24; $i++)
                    <option value={{$i}}>{{$i}}</option>
                @endfor
            </select>
            </div>
            <div class="col-md-5"></div>
        </div>
        <div class="form-group row">
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
            <label for="transferReceiver">Transfer Receiver</label>
            <select id="transferReceiver" class="custom-select custom-select-lg" name="receiver_id">
                <option selected disabled>Select user please</option>
                @foreach($receivers as $user)
                    <option value={{$user->id}}>{{$user->name}}</option>
                @endforeach
            </select>
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <input class="btn btn-primary users_sbmt col-md-4" type="submit" value="Submit">
            <div class="col-md-4"></div>
        </div>
    </form>

    <script type="text/javascript">

        $('.date').datepicker({
            format: 'dd-mm-yyyy',
            startDate: new Date()
        });

    </script>

@endsection