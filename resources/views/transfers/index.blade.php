@extends('app')
@section('content')
    <div class="raw">
        <div class="col-md-4"></div>
        <a class="btn btn-primary col-md-2" href="/transfers/fill" role="button">Create new</a>
        <div class="col-md-2"></div>
        <div class="col-md-4"></div>
    </div>
    <div class="table-responsive">
        <table class="table">
            <caption>Transfer List</caption>
            <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Receiver</th>
                <th scope="col">Amount</th>
                <th scope="col">Payment Time</th>
                <th scope="col">Created At</th>
                <th scope="col">Status</th>
            </tr>
            </thead>
            <tbody>
            @foreach($transfers as $transfer)
                <tr>
                    <td>{{ $transfer->id }}</td>
                    <td>{{ $transfer->receiver_id }}</td>
                    <td>{{ $transfer->payment_time }}</td>
                    <td>{{ $transfer->amount }}</td>
                    <td>{{ $transfer->created_at }}</td>
                    <td>{{ $transfer->status_id }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection