<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get(             '/login',               'AuthController@login')->name('login');;
Route::post(            '/auth',                'AuthController@auth');

Route::group(['middleware' => 'auth'], function () {
    Route::get(         '/',                    'UserController@index');
    Route::get(         '/logout',              'AuthController@logout');

    Route::group(['prefix' => 'transfers'], function () {
        Route::get(     '/',                    'TransferController@index');
        Route::get(     '/fill',                'TransferController@fill');
        Route::post(    '/submit',              'TransferController@submit');
    });
});