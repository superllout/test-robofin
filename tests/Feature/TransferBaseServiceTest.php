<?php

namespace Tests\Feature;

use App\Facades\TransferBasicService;
use App\Models\Transfer;
use App\Models\User;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TransferBaseServiceTest extends TestCase
{
    use DatabaseTransactions;

    public function testStore()
    {
        $date = Carbon::today()->toDateString();
        $hour = Carbon::now()->addHour()->hour;
        $payment_time = TransferBasicService::getParsedPaymentTime($date, $hour)->toDateTimeString();

        $sender = factory(User::class)->create();
        $receiver = factory(User::class)->create();
        $balance = $sender->balance;

        TransferBasicService::setTransfer($sender->id, $receiver->id, $balance, $date, $hour);
        $this->assertDatabaseHas('users', ['id' => $sender->id, 'balance' => 0]);
        $this->assertDatabaseHas('transfers', ['sender_id' => $sender->id, 'receiver_id' => $receiver->id, 'amount' => $balance, 'payment_time' => $payment_time, 'status_id' => 3]);
    }

    public function testProcess()
    {
        $transfer = factory(Transfer::class)->create();
        $receiver = User::find($transfer->receiver_id);
        $newBalance = bcadd($receiver->balance, $transfer->amount, 2);
        $this->assertDatabaseHas('transfers', ['sender_id' => $transfer->sender_id, 'receiver_id' => $transfer->receiver_id, 'amount' => $transfer->amount, 'payment_time' => $transfer->payment_time, 'status_id' => 3]);
        TransferBasicService::processAll();

        $this->assertDatabaseHas('transfers', ['sender_id' => $transfer->sender_id, 'receiver_id' => $transfer->receiver_id, 'amount' => $transfer->amount, 'payment_time' => $transfer->payment_time, 'status_id' => 1]);
        $this->assertDatabaseHas('users', ['id' => $transfer->receiver_id, 'balance' => $newBalance]);
    }

    public function testStoreFail()
    {
        $date = Carbon::today()->toDateString();
        $hour = Carbon::now()->hour;
        $payment_time = TransferBasicService::getParsedPaymentTime($date, $hour)->toDateTimeString();

        $sender = factory(User::class)->create();
        $receiver = factory(User::class)->create();
        $balance = $sender->balance + 10;
        TransferBasicService::setTransfer($sender->id, $receiver->id, $balance, $date, $hour);
        $this->assertDatabaseMissing('users', ['id' => $sender->id, 'balance' => -10.0]);
        $this->assertDatabaseMissing('transfers', ['sender_id' => $sender->id, 'receiver_id' => $receiver->id, 'amount' => $balance, 'payment_time' => $payment_time, 'status_id' => 3]);
    }
}
